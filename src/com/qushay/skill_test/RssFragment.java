package com.qushay.skill_test;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.qushay.skill_test.adapter.RssAdapter;
import com.qushay.skill_test.model.Rss;

public class RssFragment extends Fragment {
    ListView list;
    String parsedData = "";
	RssAdapter adapter;
	ArrayList<Rss> rss;
	RssHandler myRSSHandler = new RssHandler();
    Dialog rss_dialog;
    WebView web;
	public RssFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rss, container, false);
        list = (ListView)view.findViewById(R.id.lvRss);
        list.setOnItemClickListener(new OnItemClickListener() {
          	@Override
			public void onItemClick(AdapterView<?> parent, View view,int position,long id) {
          		String tDesc = adapter.getItem(position).getDescription();
          		rss_dialog = new Dialog(getActivity());
          		rss_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                rss_dialog.setContentView(R.layout.rss_dialog);
                web = (WebView)rss_dialog.findViewById(R.id.wvRss);
                web.loadData(tDesc, "text/html; charset=utf-8","utf-8");

                rss_dialog.show();
                rss_dialog.setCancelable(true);
			}
        });
        new LoadRssList().execute();
        return view;
    }
	private class LoadRssList extends AsyncTask<Void, Void, Void> {
		ProgressDialog progressDialog;

		@Override
		protected Void doInBackground(Void... params) {			
			parseXML();			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			adapter = new RssAdapter(getActivity(), myRSSHandler.itemsList);
			list.setAdapter(adapter);
			progressDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setMessage("Loading...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

	}
	private void parseXML() {        
        try {
        	URL rssUrl = new URL("http://indonesia.7langit.com/rss.xml");
            /** Handling XML */
        	 SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
        	  SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
        	  XMLReader myXMLReader = mySAXParser.getXMLReader();
        	  
        	  myXMLReader.setContentHandler(myRSSHandler);
        	  InputSource myInputSource = new InputSource(rssUrl.openStream());
        	  myXMLReader.parse(myInputSource);
 
            ArrayList<Rss> itemsList = myRSSHandler.getItemsList();
            for(int i=0;i<itemsList.size();i++){
                Rss item = itemsList.get(i);
                parsedData = parsedData + "Title: " + item.getTitle() + "\n";
//                parsedData = parsedData + "Description: " + item.getDescription() + "\n";
            }
            Log.w("AndroidParseXMLActivity", "Done");
        }
        catch (MalformedURLException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
		 } catch (ParserConfigurationException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
		 } catch (SAXException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
		 } catch (IOException e) {
		  // TODO Auto-generated catch block
		  e.printStackTrace();
		 }
        
    }
	
	public class RssHandler extends DefaultHandler {
	 
	    Boolean currentElement = false;
	    String currentValue = "";
	    Rss rss = null;
	    private ArrayList<Rss> itemsList = new ArrayList<Rss>();
	    boolean bTitle = false;
		boolean bDesc = false;
		boolean bItem = false;
	    public ArrayList<Rss> getItemsList() {
	        return itemsList;
	    }
	 
	    // Called when tag starts 
	    @Override
	    public void startElement(String uri, String localName, String qName,
	            Attributes attributes) throws SAXException {
	 
	    	
	        currentElement = true;
	        currentValue = "";
	        if (localName.equalsIgnoreCase("item")) {
	            rss = new Rss();
	            bItem=true;
	        } 
	        if (localName.equalsIgnoreCase("title")) {
				bTitle = true;
			}
	 
			if (localName.equalsIgnoreCase("description")) {
				bDesc = true;
			}
	        
	    }
	 
	    // Called when tag closing 
	    @Override
	    public void endElement(String uri, String localName, String qName)
	    throws SAXException {
	 
	        currentElement = false;
	 
	        /** set value */
	        if (localName.equalsIgnoreCase("title")&&bItem)
	            rss.setTitle(currentValue);
	        else if (localName.equalsIgnoreCase("description")&&bItem)
	            rss.setDescription(currentValue);
	        else if (localName.equalsIgnoreCase("item")){
	            itemsList.add(rss);
	            bItem=false;
	        }
	    }
	 
	    // Called to get tag characters 
	    @Override
	    public void characters(char[] ch, int start, int length)
	    throws SAXException {
	        if (currentElement&&(bTitle||bDesc)) {
	            currentValue = currentValue +  new String(ch, start, length);
	        }
	 
	    }
	 
	}
}
