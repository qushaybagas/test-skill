package com.qushay.skill_test.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.qushay.skill_test.R;
import com.qushay.skill_test.model.Tweet;

public class TweetAdapter extends ArrayAdapter<Tweet>{
	private Context context;
	private ImageLoader imageLoader;
	
	public TweetAdapter(Context context,List<Tweet> tweets){
		super(context, 0,tweets);
		this.context = context;
		initImageLoader();
	}
	
	private void initImageLoader(){
		ImageLoaderConfiguration config = ImageLoaderConfiguration.createDefault(context);
		
		ImageLoader.getInstance().init(config);
		imageLoader = ImageLoader.getInstance();
	}
	
	@Override
	public View getView (int position, View convertView, ViewGroup parent){
		View view =convertView;
		if(view==null){
			LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.tweet_listitem, null);
		}
		Tweet tweet = getItem(position);
		
		ImageView imageView = (ImageView)view.findViewById(R.id.ivProvile);
		imageLoader.displayImage(tweet.getUser().getProfileImageUrl(), imageView);
		
		TextView nameView = (TextView)view.findViewById(R.id.tvName);
		String formattedName = "<b>7langit<b><small><font color='#777'> @7langit</font></small>";
		nameView.setText(Html.fromHtml(formattedName));
		
		TextView bodyView = (TextView)view.findViewById(R.id.tvBody);
		bodyView.setText(Html.fromHtml(tweet.getBody()));
		
		TextView createdAtView = (TextView)view.findViewById(R.id.tvCreated);
		createdAtView.setText(Html.fromHtml(tweet.getCreatedAt()));
		
		return view;
		
	}
}
