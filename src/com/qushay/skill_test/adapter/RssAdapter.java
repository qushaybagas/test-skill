package com.qushay.skill_test.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.qushay.skill_test.R;
import com.qushay.skill_test.model.Rss;

public class RssAdapter extends ArrayAdapter<Rss>{
	public RssAdapter(Context context,List<Rss> rss){
		super(context, 0,rss);
	}
		
	@Override
	public View getView (int position, View convertView, ViewGroup parent){
		View view =convertView;
		if(view==null){
			LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.rss_listitem, null);
		}
		Rss rss = getItem(position);
		
		TextView nameView = (TextView)view.findViewById(R.id.tvTitle);
		nameView.setText(Html.fromHtml(rss.getTitle()));
		
		return view;
		
	}
}
