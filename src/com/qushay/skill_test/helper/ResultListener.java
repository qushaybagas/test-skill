package com.qushay.skill_test.helper;

import android.content.Context;

public interface ResultListener {
	public void onResultSuccess(Context context, String response);
	public void onResultFailed(Context context);
}
