package com.qushay.skill_test.helper;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SkillTestApp extends Application {
	private static SkillTestApp instance;
	private SharedPreferences sharedPreferences;
	
	@Override
	public void onCreate() {
		super.onCreate();

		if(instance == null){
			instance = new SkillTestApp();
			instance.sharedPreferences = getSharedPreferences("skill_test_pref", 0);
		}
	}

	public static SkillTestApp getInstance(){
		return instance;
	}

	public void setTwitterLogin(){
		Editor editor = sharedPreferences.edit();
		editor.putBoolean("twitter_login", true);
		editor.commit();
	}
	
	public boolean isTwitterLogin(){
		return sharedPreferences.getBoolean("twitter_login", false);
	}
}
