package com.qushay.skill_test.model;

import org.scribe.builder.api.Api;
import org.scribe.builder.api.TwitterApi;

import android.content.Context;

import com.codepath.oauth.OAuthBaseClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class RestClient extends OAuthBaseClient {
	public static final Class<? extends Api> REST_API_CLASS = TwitterApi.class; // Change this
	public static final String REST_URL = "https://api.twitter.com/1.1"; // Change this, base API URL
	public static final String REST_CONSUMER_KEY = "tc7nCxxoSRK9fOfSE7y6rXu6M";       // Change this
	public static final String REST_CONSUMER_SECRET = "ICOH5ogi92Gm6ipMurcuU3QNUdb42Ybzql0xh9yonyYxdOjXEq"; // Change this
	public static final String REST_CALLBACK_URL = "oauth://skill_test"; // Change this (here and in manifest)

	public RestClient(Context context) {
		super(context, REST_API_CLASS, REST_URL, REST_CONSUMER_KEY, REST_CONSUMER_SECRET, REST_CALLBACK_URL);
	}
	
	public void getHomeTimeline(int page, AsyncHttpResponseHandler handler) {
		String apiUrl = getApiUrl("statuses/user_timeline.json?screen_name=qushaybagast&count=10");
		RequestParams params = new RequestParams();
		params.put("page", String.valueOf(page));
		getClient().get(apiUrl, params, handler);
	}
	
	public void postTweet(String body, AsyncHttpResponseHandler handler) {
	    String apiUrl = getApiUrl("statuses/update.json");
	    RequestParams params = new RequestParams();
	    params.put("status", body);
	    getClient().post(apiUrl, params, handler);
	}

}