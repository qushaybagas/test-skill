package com.qushay.skill_test;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.codepath.oauth.OAuthLoginFragment;
import com.qushay.skill_test.model.RestClient;

public class TwitterLoginFragment extends OAuthLoginFragment<RestClient> implements View.OnClickListener{
	Button connect;
	OAuthLoginFragment<RestClient> auth;
   @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_twitter_login, container, false);
        connect = (Button)view.findViewById(R.id.btnTwitterLogin);
        connect.setOnClickListener(this);
        return view;
    }

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnTwitterLogin:
			getClient().connect();
			break;
		default:
			break;
		// Toast.makeText(getActivity(), "sukses",
		// Toast.LENGTH_LONG).show();
		}
	}
	@Override
	public void onLoginSuccess() {
		SkillTestApp.getInstance().setTwitterLogin();
		MainActivity mainActivity = (MainActivity) getActivity();
		mainActivity.openTwitterFragment();
	}
	@Override
	public void onLoginFailure(Exception e) {
		
	}
		
	   
   
}
