package com.qushay.skill_test;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.qushay.skill_test.adapter.TweetAdapter;
import com.qushay.skill_test.model.Tweet;
public class TwitterOfflineFragment extends Fragment {
	ListView list;
	ImageView imageView;
	ArrayList<HashMap<String, String>> tweetList;
	ArrayList<Tweet> tweets;
	TweetAdapter adapter;
	
   @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_twitter, container, false);
        list = (ListView)view.findViewById(R.id.lvTweet);
        list.setOnItemClickListener(new OnItemClickListener() {
          	@Override
			public void onItemClick(AdapterView<?> parent, View view,int position,long id) {
          		String tName = adapter.getItem(position).getUser().getName();
          		String tSceenName = adapter.getItem(position).getUser().getScreenName();
          		String tBody = adapter.getItem(position).getBody();
          		String tCreatedAt = adapter.getItem(position).getCreatedAt();
          		String tImageProfile = adapter.getItem(position).getUser().getProfileImageUrl();
          		Intent intent = new Intent(getActivity(), TwitterDetailActivity.class);
        		intent.putExtra("name", tName);
        		intent.putExtra("screenname", tSceenName);
        		intent.putExtra("body", tBody);
        		intent.putExtra("createdat", tCreatedAt);
        		intent.putExtra("imageprofile", tImageProfile);
        		startActivity(intent);
//          		Toast.makeText(getActivity(), tUser, Toast.LENGTH_LONG).show();
			}
        });
		new TweetsTask().execute();
        return view;
    }
   
   
   private class TweetsTask extends AsyncTask<Void, Void, Void> {
		ProgressDialog progressDialog;
		
		@Override
		protected Void doInBackground(Void... params) {		

			try{
				
				JSONArray jArray = new JSONArray(loadJSONFromAsset());
				tweets= Tweet.fromJson(jArray);
			}catch(Throwable t){
				
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			adapter = new TweetAdapter(getActivity(), tweets);
			list.setAdapter(adapter);
			progressDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setMessage("Loading...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}
		
		public String loadJSONFromAsset() {
		    String json = null;
		    try {

		        InputStream is = getActivity().getAssets().open("timeline_7langit.json");

		        int size = is.available();

		        byte[] buffer = new byte[size];

		        is.read(buffer);

		        is.close();

		        json = new String(buffer, "UTF-8");


		    } catch (IOException ex) {
		        ex.printStackTrace();
		        return null;
		    }
		    return json;

		}
	}
   
    
}
