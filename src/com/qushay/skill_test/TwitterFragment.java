package com.qushay.skill_test;


import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.qushay.skill_test.adapter.TweetAdapter;
import com.qushay.skill_test.model.Tweet;
public class TwitterFragment extends Fragment {
	ListView list;
	ImageView imageView;
	ArrayList<HashMap<String, String>> tweetList;
	ArrayList<Tweet> tweets;
	TweetAdapter adapter;
	private final String API_KEY="tc7nCxxoSRK9fOfSE7y6rXu6M";
	private final String API_SECRET="ICOH5ogi92Gm6ipMurcuU3QNUdb42Ybzql0xh9yonyYxdOjXEq";
	private final String TOKEN_KEY="117809255-FskKwDcERNcqb1NasNjXsZdEGZa4FaClj2dA4LW5";
	private final String TOKEN_SECRET="Qa10YYPuOwBLhxZ58kyuVpc45vndkFINBGY6NbQshj0JU";
	String twitterAPI= "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=7langit&count=10";
	
	@Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_twitter, container, false);
        list = (ListView)view.findViewById(R.id.lvTweet);
        list.setOnItemClickListener(new OnItemClickListener() {
          	@Override
			public void onItemClick(AdapterView<?> parent, View view,int position,long id) {

          		Long tId = adapter.getItem(position).getId();
          		String tName = adapter.getItem(position).getUser().getName();
          		String tSceenName = adapter.getItem(position).getUser().getScreenName();
          		String tBody = adapter.getItem(position).getBody();
          		String tCreatedAt = adapter.getItem(position).getCreatedAt();
          		String tImageProfile = adapter.getItem(position).getUser().getProfileImageUrl();
          		Intent intent = new Intent(getActivity(), TwitterDetailActivity.class);
          		intent.putExtra("uid", tId);
        		intent.putExtra("name", tName);
        		intent.putExtra("screenname", tSceenName);
        		intent.putExtra("body", tBody);
        		intent.putExtra("createdat", tCreatedAt);
        		intent.putExtra("imageprofile", tImageProfile);
        		startActivity(intent);
//          		Toast.makeText(getActivity(), tUser, Toast.LENGTH_LONG).show();
			}
        });
		new TweetsTask().execute();
        return view;
    }
   
   
   private class TweetsTask extends AsyncTask<Void, Void, Void> {
		ProgressDialog progressDialog;

		@Override
		protected Void doInBackground(Void... params) {			
			OAuthService service = new ServiceBuilder()
	            .provider(TwitterApi.SSL.class)
	            .apiKey(API_KEY)
	            .apiSecret(API_SECRET)
	            .build();
			Token accessToken= new Token(TOKEN_KEY, TOKEN_SECRET);
			OAuthRequest request = new OAuthRequest(Verb.GET, twitterAPI);
			service.signRequest(accessToken, request);
			Response response = request.send();
			System.out.println(response.getBody());

			try{
				JSONArray jArray = new JSONArray(response.getBody());
				tweets= Tweet.fromJson(jArray);
			}catch(Throwable t){
				
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			adapter = new TweetAdapter(getActivity(), tweets);
			list.setAdapter(adapter);
			progressDialog.dismiss();
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setMessage("Loading...");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

	}
   
    
}
