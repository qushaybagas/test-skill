package com.qushay.skill_test;

import java.util.ArrayList;
import java.util.HashMap;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.OAuthAuthorization;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.qushay.skill_test.adapter.TweetAdapter;
import com.qushay.skill_test.helper.Token;
import com.qushay.skill_test.model.Tweet;

public class TwitterDetailActivity extends Activity {
	TextView tvName,tvScreenName,tvBody,tvCreatedAt,tvReplyCounter;
	ImageView ivProfile,ivRetweet;
	EditText etReply;
	LinearLayout layReply;
	private ImageLoader imageLoader;
	
	Long uid;
	boolean isReply=false;
	boolean isRetweet=false;
	boolean isRetweeted=false;
	ListView list;
	ImageView imageView;
	ArrayList<HashMap<String, String>> tweetList;
	ArrayList<Tweet> tweets;
	TweetAdapter adapter;
	String API_KEY;
	String API_SECRET;
	String TOKEN_KEY;
	String TOKEN_SECRET;
	String twitterAPI= "https://api.twitter.com/1.1/statuses/update.json";
	String payloadAPI ="trim_user=true&include_entities=true";
	
	AccessToken accessToken;
	OAuthAuthorization authorization;
	Twitter twitter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_detail);
		Token tok = new Token(getApplicationContext());
		API_KEY=tok.getAPI();
		API_SECRET=tok.getAPISecret();
		TOKEN_KEY = tok.getToken();
		TOKEN_SECRET=tok.getTokenSecret();
		
    	ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setOAuthConsumerKey(API_KEY);
		configurationBuilder.setOAuthConsumerSecret(API_SECRET);
		configurationBuilder.setOAuthAccessToken(TOKEN_KEY);
		configurationBuilder.setOAuthAccessTokenSecret(TOKEN_SECRET);
		Configuration configuration = configurationBuilder.build();
		twitter = new TwitterFactory(configuration).getInstance();

		initImageLoader();
		tvName = (TextView)findViewById(R.id.tvName);
		tvScreenName =(TextView)findViewById(R.id.tvScreenName);
		tvBody =(TextView)findViewById(R.id.tvBody);
		tvCreatedAt =(TextView)findViewById(R.id.tvCreated);
		ivProfile= (ImageView)findViewById(R.id.ivProvile);
		ivRetweet=(ImageView)findViewById(R.id.btRetweet);
		etReply=(EditText)findViewById(R.id.etReply);
		layReply=(LinearLayout)findViewById(R.id.layoutReply);
		tvReplyCounter=(TextView)findViewById(R.id.tvReplyTextCounter);
		handleIntent();
		new updateTwitterStatus().execute();
		etReply.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
			    if(hasFocus){
			        etReply.setText("@7langit ");
			        layReply.setVisibility(View.VISIBLE);
			    }else{
			    	etReply.setHint("Reply");
			        layReply.setVisibility(View.GONE);
			    }
			}
		});
		etReply.addTextChangedListener(new TextWatcher() {
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start,
			     int before, int count) {
				int counter= 140;
				if(s.length() != 0)
					counter=counter-s.length();
			    	tvReplyCounter.setText(counter+"");
			   	}

				@Override
				public void afterTextChanged(Editable s) {
					
				}
			});
		
		}
	private void handleIntent(){
		Intent intent = getIntent();
		Long tId = intent.getLongExtra("uid", 0);
		String tName = intent.getStringExtra("name");
		String tScreenName = intent.getStringExtra("screenname");
		String tBody = intent.getStringExtra("body");
		String tCreatedAt = intent.getStringExtra("createdat");
		String tImageProfile = intent.getStringExtra("imageprofile");
		uid= tId;
		tvName.setText(tName);
		tvScreenName.setText("@"+tScreenName);
		tvBody.setText(tBody);
		tvCreatedAt.setText(tCreatedAt);
		imageLoader.displayImage(tImageProfile, ivProfile);
	}
	
	private void initImageLoader(){		
		ImageLoaderConfiguration config = ImageLoaderConfiguration.createDefault(getApplicationContext());
		
		ImageLoader.getInstance().init(config);
		imageLoader = ImageLoader.getInstance();
		
	}
	
	
	public void onClickReply(View v) {
		isReply=true;
		new updateTwitterStatus().execute();
	}
	
	public void onClickRetweet(View v) {
		isRetweet=true;
		new updateTwitterStatus().execute();
	}
	
	class updateTwitterStatus extends AsyncTask<String, String, String> {
		AccessToken accessToken;
		StatusUpdate ad;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        
        protected String doInBackground(String... args) {
        	
    		try {
    			if(isReply){
    				twitter.updateStatus(new StatusUpdate(etReply.getText().toString()).inReplyToStatusId(uid));
    			}
    			else if(isRetweet){
    				
    				isRetweeted=twitter.showStatus(uid).isRetweeted();
    				if(isRetweeted){
    					twitter.destroyStatus(uid);
    				}else{
    					twitter.retweetStatus(uid);
    				}
    			}
    			else{
    				isRetweeted=twitter.showStatus(uid).isRetweeted();
    			}
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		return null;
        }

        /**
         * After completing background task Dismiss the progress dialog and show
         * the data in UI Always use runOnUiThread(new Runnable()) to update UI
         * from background thread, otherwise you will get error
         * **/
        protected void onPostExecute(String file_url) {
        	if (isReply){
    			Toast.makeText(getApplicationContext(), "Send", Toast.LENGTH_SHORT).show();
    			etReply.setText("");
    			etReply.clearFocus();
    			InputMethodManager imm = (InputMethodManager)getSystemService(
    				      Context.INPUT_METHOD_SERVICE);
    			imm.hideSoftInputFromWindow(etReply.getWindowToken(), 0);
    			isReply=false;
        	}else if(isRetweet){
            	if(isRetweeted){
		    		ivRetweet.setImageResource(R.drawable.retweet);
		    		Toast.makeText(getApplicationContext(), "UnRetweet", Toast.LENGTH_SHORT).show();
		    		
				}else{
		    		ivRetweet.setImageResource(R.drawable.retweet_on);
		    		Toast.makeText(getApplicationContext(), "Retweet", Toast.LENGTH_SHORT).show();
				}
    			
    			isRetweet=false;
        	}else{
        		if(isRetweeted){
		    		ivRetweet.setImageResource(R.drawable.retweet_on);
        		}else{
		    		ivRetweet.setImageResource(R.drawable.retweet);
		    	}
        	}
			
        	
        }

    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    // Respond to the action bar's Up/Home button
	    case android.R.id.home:
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
	    }
	    return super.onOptionsItemSelected(item);
	}
}
